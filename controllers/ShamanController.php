<?php
namespace app\controllers;

use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\ShamanLoginForm;

class ShamanController extends Controller
{
    public function actionIndex()
    {
        $model = new ShamanLoginForm();
        return $this->render('login',['model' => $model]);
    }
    public function actionLogin()
    {
        $request = \Yii::$app->request;
        if ($request->isPost)
        {
            $rsp = \app\services\ShamanService::getToken($request->post());
            return $rsp;
        }
    }

    public function actionUser()
    {
        $request = \Yii::$app->request;
        if ($request->isPost)
        {
            $rsp = \app\services\ShamanService::getUserByToken($request->post('token'));
            return $rsp;
        }
    }

    public function actionInfoSecretKey()
    {
        $request = \Yii::$app->request;
        if ($request->isPost)
        {
            $rsp = \app\services\ShamanService::getInfoSecretKey($request->post('secretKey'));
            return $rsp;
        }
    }
    
    
}
