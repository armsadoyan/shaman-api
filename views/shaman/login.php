<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<div class="site-login">
    <h1>Login int shaman api</h1>


    <div class="row">
        <div class="col-lg-5">
            <form class="shaman__api__login__form">
                <p>Email : <input type="text" name="email" value="test@user.demo"/></p>
                <p>Password: <input type="text" name="password" value="1234567A"/></p>
                <p>Curlname: <input type="text" name="curlname" value="web-2015"/></p>

            </form>
            <button class="btn btn-success shaman__api__login__btn">Login int shaman api</button>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script>
$('.shaman__api__login__btn')
    .click(function(event) {
        var form = $('.shaman__api__login__form');
        $.ajax({
            url  : 'shaman/login',
            type : 'POST',
            data : $(form).serializeArray(),
            success: function(data) {
                data = JSON.parse(data);
                if(!data || typeof data.token == 'undefined')
                {
                    var html = '<p>'+data.name+'</p>';
                    html +='<p>'+data.message.email+'</p>';
                    $('#myModal .modal-body').text(html);
                    $('#myModal').modal('show');
                    console.log(data);
                    return false;
                }
                $.ajax({
                    url  : 'shaman/user',
                    type : 'POST',
                    data : { token:data.token },
                    success: function(data)
                    {
                        data = JSON.parse(data);
                        
                        var html = '';
                        if(!data || typeof data.id == 'undefined')
                        {
                            html += data.message;
                        } else {
                            html += 'Success Info';
                            html += '<p>ID: '    + data.id    + '</p>';
                            html += '<p>name: '  + data.name  + '</p>';
                            html += '<p>email: ' + data.email + '</p>';
                        }
                        $('#myModal .modal-body').text(html);
                        $('#myModal').modal('show');
                        console.log(data);
                    }
                });
            }
        });
    });
</script>

