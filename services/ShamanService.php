<?php
namespace app\services;

class ShamanService {
    
    /**
     * Login into https://api.shamandev.com
     * 
     * @post array $post input Post data
     * @return json $rsp response from api
     */
    public static function getToken($post)
    {
        $access = [
            'url' => 'https://api.shamandev.com/auth/login',
            'post_data' => $post,
            'headers' => []
        ];
        $rsp = self::sendCURL($access);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $rsp;
    }
    
    /**
     * 
     * @param string $token 
     */
    public static function getUserByToken($token)
    {
        $access = [
            'url' => 'https://api.shamandev.com/user/current',
            'post_data' => [
                'token' => $token
            ],
            'headers' => [
                'Authorization: Bearer '.$token
            ]
        ];
        $rsp = self::sendCURL($access, 'GET');
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $rsp;
    }
    
    /**
     * 
     * @param string $token 
     */
    public static function getInfoSecretKey($secretKey)
    {
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $rsp;
    }
    
    /**
     * 
     * @param string $token 
     */
    public static function sendCURL($data, $method = 'POST')
    {
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $data['url']); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $data['headers']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if($method === 'POST')
        {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data['post_data']);
        }
        $rsp = curl_exec($ch); 
        curl_close($ch);
        
        return $rsp;
    }
    
    
}