<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\ShamanUser;
/**
 * ShamanLoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ShamanLoginForm extends Model
{
    public $email;
    public $password;
    public $curlname;
    public $rememberMe = true;
    
    private $_user= false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email', 'password','curlname'], 'required'],
            ['email', 'email'],
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }
    
    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    private function getUser()
    {
        if ($this->_user === false) {
            $this->_user = ShamanUser::findByEmail($this->email);
                        
        }
        return $this->_user;
    }
}
