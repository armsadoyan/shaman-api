<?php

namespace app\models;

class ShamanUser extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $id;
    public $email;
    public $password;
    public $curlname;
    public $authKey;
    public $accessToken;

    private static $users = [
        '100' =>[
                'id' => '1',
                'email' => 'test@user.demo',
                'password' => '1234567A',
                'curlname' => 'web-2015',
                'authKey'  => 'test100key',
                'accessToken' => '100-token',
            ]
        ];
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        foreach (self::$users as $user)
        {
            if($user['id'] == $id)
                return new static($user);
            return null;
        }
    }
    
    public static function findByEmail($email)
    {
        foreach (self::$users as $user)
        {
            if($user['email'] == $email)
                return new static($user);
            return null;
        }
    }
    
}
